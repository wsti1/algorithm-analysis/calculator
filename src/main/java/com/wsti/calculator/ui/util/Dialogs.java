package com.wsti.calculator.ui.util;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;


public class Dialogs {

    private Dialogs() {}

    public static void showErrorDialog(String message) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Invalid operation");
        alert.setContentText(message);
        alert.showAndWait();
    }
}
