package com.wsti.calculator.ui.controller;

import com.wsti.calculator.model.CalculatorEntryModel;
import com.wsti.calculator.model.CalculatorExpressionModel;
import com.wsti.calculator.model.token.util.Functions;
import com.wsti.calculator.ui.util.Dialogs;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.net.URL;
import java.util.ResourceBundle;

@Slf4j
public class CalculatorController implements Initializable {

    @FXML
    private TextField entryTextField;

    @FXML
    private TextField expressionTextField;

    private CalculatorEntryModel entryModel;
    private CalculatorExpressionModel expressionModel;

    public CalculatorController() {
        this.entryModel = new CalculatorEntryModel();
        this.expressionModel = new CalculatorExpressionModel();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        entryTextField.textProperty().bind(entryModel.getProperty());
        expressionTextField.textProperty().bind(expressionModel.getProperty());
    }

    @FXML
    public void clearAll() {
        entryModel.clearEntry();
        expressionModel.clearExpression();
    }

    @FXML
    public void clearEntry() {
        entryModel.clearEntry();
    }

    @FXML
    public void removeEntry() {
        entryModel.removeEntry();
    }

    @FXML
    public void negateEntry() {
        entryModel.negateEntry();
    }

    @FXML
    public void setComma() {
        entryModel.enableFloatingEntry();
    }

    @FXML
    public void addNumber(ActionEvent event) {
        String number = getAccessibleText(event);
        entryModel.concatEntry(number);
    }

    @FXML
    public void addConstantNumber(ActionEvent event) {
        String constantNumber = getAccessibleText(event);
        entryModel.setEntry(constantNumber);
    }

    @FXML
    public void addFunction(ActionEvent event) {
        String functionName = getAccessibleText(event);
        if (expressionModel.isEndingWithCalculatedToken()) {
            expressionModel.clearExpression();
        }
        if (expressionModel.isEmpty() || expressionModel.isEndingWithOperator() || expressionModel.isEndingWithOpenParenthesis()) {
            expressionModel.addTokens(Functions.toToken(functionName, entryModel.getEntry()));
            entryModel.clearEntry();
        }
    }

    @FXML
    public void addOpenParenthesis(ActionEvent event) {
        String parenthesis = getAccessibleText(event);
        if (expressionModel.isEmpty() || expressionModel.isEndingWithOperator() || expressionModel.isEndingWithOpenParenthesis()) {
            expressionModel.addTokens(parenthesis);
        }
    }

    @FXML
    public void addClosedParenthesis(ActionEvent event) {
        String parenthesis = getAccessibleText(event);
        if (expressionModel.getUnclosedParenthesisCount() > 0) {
            if (expressionModel.isEndingWithOperator() || expressionModel.isEndingWithOpenParenthesis()) {
                expressionModel.addTokens(entryModel.getEntry());
            }
            if (!expressionModel.isEndingWithOperator()) {
                expressionModel.addTokens(parenthesis);
                entryModel.setEntered(false);
            }
        }
    }

    @FXML
    public void addOperator(ActionEvent event) {
        String operator = getAccessibleText(event);
        if (expressionModel.isEndingWithCalculatedToken()) {
            expressionModel.clearExpression();
            expressionModel.addTokens(entryModel.getEntry(), operator);
            entryModel.clearEntry();

        } else if (!entryModel.isEntered() && expressionModel.isEndingWithOperator()) {
            expressionModel.replaceLastToken(operator);

        } else if (expressionModel.isEndingWithFunction() || expressionModel.isEndingWithClosedParenthesis()) {
            expressionModel.addTokens(operator);

        } else if (!expressionModel.isEmpty() && !expressionModel.isEndingWithOperator() && expressionModel.isEndingWithClosedParenthesis()) {
            expressionModel.addTokens(operator, entryModel.getEntry());
            entryModel.clearEntry();
        }
        else if (entryModel.isEntered()) {
            expressionModel.addTokens(entryModel.getEntry(), operator);
            entryModel.clearEntry();
        }
    }

    protected String getAccessibleText(Event event) {
        return ((Button) event.getSource()).getAccessibleText();
    }

    @FXML
    public void calculate() {
        if (expressionModel.isEndingWithCalculatedToken()) {
            expressionModel.clearExpression();
        }
        if (entryModel.isEntered() && !expressionModel.isEndingWithFunction() && !expressionModel.isEndingWithClosedParenthesis()) {
            expressionModel.addTokens(entryModel.getEntry());
        }
        if ((!expressionModel.isEmpty() && !expressionModel.isEndingWithOperator()) || expressionModel.isEndingWithClosedParenthesis()) {
            try {
                BigDecimal result = expressionModel.calculate();
                entryModel.setEntry(result.toString());

            } catch (Exception e) {
                log.error("Error details: {}", e.getMessage());
                Dialogs.showErrorDialog(e.getMessage());
            }
        }
    }
}
