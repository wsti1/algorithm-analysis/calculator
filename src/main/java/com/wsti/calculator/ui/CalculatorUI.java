package com.wsti.calculator.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CalculatorUI extends Application {

    private static final String FXML_URI = "/calculator.fxml";

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(FXML_URI));
        Parent root = loader.load();

        stage.setTitle("Calculator");
        stage.setScene(new Scene(root));
        stage.show();
    }
}
