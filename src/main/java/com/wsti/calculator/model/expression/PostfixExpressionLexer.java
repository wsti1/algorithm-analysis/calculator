package com.wsti.calculator.model.expression;

import com.wsti.calculator.model.exception.LexerException;
import com.wsti.calculator.model.token.Number;
import com.wsti.calculator.model.token.Token;
import com.wsti.calculator.model.token.util.*;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class PostfixExpressionLexer {

    public List<Token> analyze(String expression) throws LexerException {
        List<Token> tokens = new ArrayList<>();

        for (String token : expression.split(" ")) {
            if (Numbers.isNumber(token)) {
                tokens.add(new Number(token));

            } else if (Operators.isOperator(token)) {
                tokens.add(Operators.of(token));

            } else if (Parentheses.isParenthesis(token)) {
                tokens.add(Parentheses.of(token));

            } else if (Functions.isFunction(token)) {
                tokens.add(Functions.of(token));

            } else if (isNotCalculatedToken(token)) {
                throw new LexerException("Unrecognized token: " + token);
            }
        }
        log.info("Analyzed tokens: {}", Strings.join(", ", tokens));
        return tokens;
    }

    private boolean isNotCalculatedToken(String token) {
        return !"=".equals(token);
    }
}
