package com.wsti.calculator.model.expression;

import com.wsti.calculator.model.exception.ParserException;
import com.wsti.calculator.model.token.Function;
import com.wsti.calculator.model.token.Number;
import com.wsti.calculator.model.token.Operator;
import com.wsti.calculator.model.token.Token;
import com.wsti.calculator.model.token.parenthesis.ClosedParenthesis;
import com.wsti.calculator.model.token.parenthesis.OpenParenthesis;
import com.wsti.calculator.model.token.util.Parentheses;
import com.wsti.calculator.model.token.util.Strings;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Stack;

@Slf4j
public class PostfixExpressionParser {

    private Stack<Token> stack;
    private Stack<Token> output;

    public PostfixExpressionParser() {
        this.stack = new Stack<>();
        this.output = new Stack<>();
    }

    public Stack<Token> parse(List<Token> tokens) throws ParserException {
        clearStackAndOutput();

        for (int i = 0; i < tokens.size(); i++) {
            Token token = tokens.get(i);

            if (i < (tokens.size() - 1)) {
                validateTokens(token, tokens.get(i + 1));
            }
            processToken(token);
        }
        moveStackedTokensToOutput();
        log.info("Parsed tokens: {}", Strings.join(", ", output));
        return output;
    }

    private void clearStackAndOutput() {
        output.clear();
        stack.clear();
    }

    private void validateTokens(Token currentToken, Token nextToken) throws ParserException {
        if ((currentToken instanceof Number) && (nextToken instanceof Number)) {
            throw new ParserException(String.format("Token %s and %s must be separated by operator", currentToken, nextToken));
        }
        if ((currentToken instanceof Operator) && (nextToken instanceof Operator)) {
            throw new ParserException(String.format("Token %s and %s must be separated by number or function", currentToken, nextToken));
        }
    }

    private void processToken(Token token) throws ParserException {
        if (token instanceof Number) {
            output.push(token);

        } else if (token instanceof Function) {
            output.push(token);

        } else if (token instanceof OpenParenthesis) {
            stack.push(token);

        } else if (token instanceof ClosedParenthesis) {
            moveStackedTokensToOutputUntilOpenParenthesisIsFound();
            stack.pop();

        } else if (token instanceof Operator) {
            Operator operator = (Operator) token;
            moveStackedTokensWithGreaterOrEqualPriorityToOutput(operator);
            stack.push(operator);

        } else {
            throw new ParserException("Unrecognized token: " + token);
        }
    }

    private void moveStackedTokensToOutputUntilOpenParenthesisIsFound() throws ParserException {
        while (!stack.isEmpty() && !Parentheses.OPEN_PARENTHESIS.equals(stack.peek())) {
            output.push(stack.pop());
        }
        if (stack.isEmpty()) {
            throw new ParserException("Missing parenthesis: (");
        }
    }

    private void moveStackedTokensWithGreaterOrEqualPriorityToOutput(Operator operator) {
        while (!stack.isEmpty() && operator.getPriority() <= getTopTokenPriority()) {
            output.push(stack.pop());
        }
    }

    private Integer getTopTokenPriority() {
        return stack.peek() instanceof OpenParenthesis ? 0 : ((Operator) stack.peek()).getPriority();
    }

    private void moveStackedTokensToOutput() throws ParserException {
        while (!stack.isEmpty()) {
            if (stack.peek() instanceof OpenParenthesis) {
                throw new ParserException("Missing parenthesis: )");
            }
            output.push(stack.pop());
        }
    }
}
