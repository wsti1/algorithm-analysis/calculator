package com.wsti.calculator.model.expression;

import com.wsti.calculator.model.exception.InterpreterException;
import com.wsti.calculator.model.token.Function;
import com.wsti.calculator.model.token.Number;
import com.wsti.calculator.model.token.Operator;
import com.wsti.calculator.model.token.Token;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Stack;

@Slf4j
public class PostfixExpressionInterpreter {

    public BigDecimal compute(Stack<Token> tokens) throws InterpreterException {
        Stack<Number> stack = new Stack<>();

        for (Token token : tokens) {
            if (token instanceof Number) {
                stack.push((Number) token);

            } else if (token instanceof Operator) {
                Operator operator = (Operator) token;

                Number secondNumber = stack.pop();
                Number firstNumber = stack.pop();
                stack.push(operator.apply(firstNumber, secondNumber));

            } else if (token instanceof Function) {
                Function function = (Function) token;
                stack.push(function.get());

            } else {
                throw new InterpreterException("Unrecognized token: " + token);
            }
        }
        BigDecimal result = formatResult(stack.pop().getValue());
        log.info("Computed result: {}", result.toString());
        return result;
    }

    private BigDecimal formatResult(BigDecimal result) {
        return new BigDecimal(result.stripTrailingZeros().toPlainString());
    }
}
