package com.wsti.calculator.model;

import com.wsti.calculator.model.exception.InterpreterException;
import com.wsti.calculator.model.exception.LexerException;
import com.wsti.calculator.model.exception.ParserException;
import com.wsti.calculator.model.expression.PostfixExpressionInterpreter;
import com.wsti.calculator.model.expression.PostfixExpressionLexer;
import com.wsti.calculator.model.expression.PostfixExpressionParser;
import com.wsti.calculator.model.token.Token;
import com.wsti.calculator.model.token.util.Functions;
import com.wsti.calculator.model.token.util.Operators;
import com.wsti.calculator.model.token.util.Parentheses;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class CalculatorExpressionModel {

    private ObservableList<String> list;
    private StringProperty property;
    private PostfixExpressionLexer lexer;
    private PostfixExpressionParser parser;
    private PostfixExpressionInterpreter compiler;

    public CalculatorExpressionModel() {
        this.property = new SimpleStringProperty("");
        this.lexer = new PostfixExpressionLexer();
        this.parser = new PostfixExpressionParser();
        this.compiler = new PostfixExpressionInterpreter();
        this.list = FXCollections.observableArrayList();
        this.list.addListener((ListChangeListener<String>) change -> property.setValue(String.join(" ", list)));
    }

    public StringProperty getProperty() {
        return property;
    }

    public void clearExpression() {
        list.clear();
    }

    public void addTokens(String... tokens) {
        list.addAll(Arrays.asList(tokens));
    }

    public void replaceLastToken(String token) {
        list.remove(list.size() - 1);
        list.add(token);
    }

    public boolean isEndingWithOperator() {
        return !isEmpty() && Operators.isOperator(getLastElement());
    }

    public boolean isEndingWithFunction() {
        return !isEmpty() && Functions.isFunction(getLastElement());
    }

    public boolean isEndingWithOpenParenthesis() {
        return !isEmpty() && Parentheses.OPEN_PARENTHESIS.getIdentifier().equals(getLastElement());
    }

    public boolean isEndingWithClosedParenthesis() {
        return !isEmpty() && Parentheses.CLOSED_PARENTHESIS.getIdentifier().equals(getLastElement());
    }

    public boolean isEndingWithCalculatedToken() {
        return !isEmpty() && "=".equals(getLastElement());
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    private String getLastElement() {
        return list.get(list.size() - 1);
    }

    public int getUnclosedParenthesisCount() {
        return Collections.frequency(list, "(") - Collections.frequency(list, ")");
    }

    public BigDecimal calculate() throws LexerException, ParserException, InterpreterException {
        list.add("=");
        String expression = property.getValue();

        List<Token> tokens = lexer.analyze(expression);
        Stack<Token> stackedTokens = parser.parse(tokens);
        return compiler.compute(stackedTokens);
    }
}
