package com.wsti.calculator.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.math.BigDecimal;

public class CalculatorEntryModel {

    private static final String INITIAL_ENTRY_VALUE = "0";

    private EntryHolder entry;

    public CalculatorEntryModel() {
        this.entry = new EntryHolder(INITIAL_ENTRY_VALUE);
    }

    public StringProperty getProperty() {
        return entry;
    }

    public String getEntry() {
        String value = entry.getValue();
        if (isFloatingEntry() && (value.length() == (value.indexOf('.') + 1))) {
            return value.concat("0");
        }
        return value;
    }

    private boolean isFloatingEntry() {
        return entry.getValue().contains(".");
    }

    public void setEntry(String value) {
        entry.setValue(value);
    }

    public void clearEntry() {
        entry.setValue(INITIAL_ENTRY_VALUE);
        setEntered(false);
    }

    public void removeEntry() {
        String value = entry.getValue();
        if (value.length() > 1) {
            entry.setValue(value.substring(0, value.length() - 1));
        } else {
            entry.setValue(INITIAL_ENTRY_VALUE);
        }
    }

    public void negateEntry() {
        if (!isInitialEntry()) {
            entry.setValue(new BigDecimal(entry.getValue()).negate());
        }
    }

    public void enableFloatingEntry() {
        if (!entry.getValue().contains(".")) {
            entry.setValue(entry.getValue().concat("."));
        }
    }

    public boolean isEntered() {
        return entry.isEntered;
    }

    public void setEntered(boolean entered) {
        entry.isEntered = entered;
    }

    public void concatEntry(String value) {
        entry.setValue(isInitialEntry() ? value : entry.getValue().concat(value));
    }

    private boolean isInitialEntry() {
        return entry.getValue().equals(INITIAL_ENTRY_VALUE);
    }

    private static class EntryHolder extends SimpleStringProperty {

        private boolean isEntered;

        public EntryHolder(String initialValue) {
            super(initialValue);
            this.isEntered = false;
        }

        public void setValue(BigDecimal number) {
            setValue(number.toString());
        }

        @Override
        public void setValue(String value) {
            super.setValue(value);
            isEntered = true;
        }
    }
}
