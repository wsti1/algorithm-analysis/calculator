package com.wsti.calculator.model.token.parenthesis;

import com.wsti.calculator.model.token.Parenthesis;

public class ClosedParenthesis extends Parenthesis {

    public ClosedParenthesis() {
        super(")");
    }
}
