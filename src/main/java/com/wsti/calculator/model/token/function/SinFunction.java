package com.wsti.calculator.model.token.function;

import com.wsti.calculator.model.token.Function;
import com.wsti.calculator.model.token.Number;

import java.math.BigDecimal;

public class SinFunction extends Function {

    public SinFunction(Number argument) {
        super("sin", argument);
    }

    @Override
    public Number get() {
        BigDecimal degrees = argument.getValue();
        double radians = Math.toRadians(degrees.doubleValue());
        return new Number(Math.sin(radians));
    }
}