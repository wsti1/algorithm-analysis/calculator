package com.wsti.calculator.model.token;

public abstract class Token {

    protected String identifier;

    public Token(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    @Override
    public String toString() {
        return identifier;
    }
}
