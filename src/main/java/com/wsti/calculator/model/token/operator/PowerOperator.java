package com.wsti.calculator.model.token.operator;

import com.wsti.calculator.model.token.Number;
import com.wsti.calculator.model.token.Operator;

import java.math.BigDecimal;
import java.math.MathContext;

public class PowerOperator extends Operator {

    public PowerOperator() {
        super("^", 3);
    }

    @Override
    public Number apply(Number firstNumber, Number secondNumber) {
        BigDecimal firstValue = firstNumber.getValue();
        BigDecimal secondValue = secondNumber.getValue();

        return new Number(firstValue.pow(secondValue.intValue(), MathContext.DECIMAL32));
    }
}