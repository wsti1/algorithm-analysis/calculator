package com.wsti.calculator.model.token.function;

import com.wsti.calculator.model.token.Function;
import com.wsti.calculator.model.token.Number;

import java.math.BigDecimal;

public class CtgFunction extends Function {

    public CtgFunction(Number argument) {
        super("ctg", argument);
    }

    @Override
    public Number get() {
        BigDecimal degrees = argument.getValue();
        double radians = Math.toRadians(degrees.doubleValue());
        return new Number(1.0 / Math.tan(radians));
    }
}