package com.wsti.calculator.model.token.util;

import com.wsti.calculator.model.token.Parenthesis;
import com.wsti.calculator.model.token.parenthesis.ClosedParenthesis;
import com.wsti.calculator.model.token.parenthesis.OpenParenthesis;

public class Parentheses {

    public static final OpenParenthesis OPEN_PARENTHESIS = new OpenParenthesis();
    public static final ClosedParenthesis CLOSED_PARENTHESIS = new ClosedParenthesis();

    private Parentheses() {}

    public static Parenthesis of(String token) {
        switch (token) {
            case "(":
                return OPEN_PARENTHESIS;
            case ")":
                return CLOSED_PARENTHESIS;
            default:
                throw new IllegalArgumentException("Token: " + token + " is not a parenthesis");
        }
    }

    public static boolean isParenthesis(String token) {
        return OPEN_PARENTHESIS.getIdentifier().equals(token) || CLOSED_PARENTHESIS.getIdentifier().equals(token);
    }
}
