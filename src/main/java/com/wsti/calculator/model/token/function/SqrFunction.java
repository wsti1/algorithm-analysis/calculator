package com.wsti.calculator.model.token.function;

import com.wsti.calculator.model.token.Function;
import com.wsti.calculator.model.token.Number;

public class SqrFunction extends Function {

    public SqrFunction(Number argument) {
        super("sqr", argument);
    }

    @Override
    public Number get() {
        return new Number(argument.getValue().pow(2));
    }
}