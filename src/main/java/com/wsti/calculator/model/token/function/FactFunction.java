package com.wsti.calculator.model.token.function;

import com.wsti.calculator.model.token.Function;
import com.wsti.calculator.model.token.Number;

import java.math.BigInteger;

import static java.math.BigInteger.ONE;

public class FactFunction extends Function {

    public FactFunction(Number argument) {
        super("fact", argument);
    }

    @Override
    public Number get() {
        BigInteger factorial = ONE;

        for (BigInteger i = ONE; compareArgumentTo(i) <= 0; i = i.add(ONE)) {
            factorial = factorial.multiply(i);
        }
        return new Number(factorial.toString());
    }

    private int compareArgumentTo(BigInteger iterator) {
        return iterator.compareTo(argument.getValue().toBigInteger());
    }
}