package com.wsti.calculator.model.token.util;

public final class Numbers {

    private static final String NUMBER_REGEXP = "-?\\d+(\\.\\d+)?";

    private Numbers() {}

    public static boolean isNumber(String token) {
        return token.matches(NUMBER_REGEXP);
    }
}
