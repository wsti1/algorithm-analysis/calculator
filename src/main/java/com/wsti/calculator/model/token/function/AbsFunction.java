package com.wsti.calculator.model.token.function;

import com.wsti.calculator.model.token.Function;
import com.wsti.calculator.model.token.Number;

public class AbsFunction extends Function {

    public AbsFunction(Number argument) {
        super("abs", argument);
    }

    @Override
    public Number get() {
        return new Number(argument.getValue().abs());
    }
}