package com.wsti.calculator.model.token.operator;

import com.wsti.calculator.model.token.Number;
import com.wsti.calculator.model.token.Operator;

import java.math.BigDecimal;

public class MultiplyOperator extends Operator {

    public MultiplyOperator() {
        super("*", 2);
    }

    @Override
    public Number apply(Number firstNumber, Number secondNumber) {
        BigDecimal firstValue = firstNumber.getValue();
        BigDecimal secondValue = secondNumber.getValue();

        return new Number(firstValue.multiply(secondValue));
    }
}