package com.wsti.calculator.model.token.util;

import com.wsti.calculator.model.token.Function;
import com.wsti.calculator.model.token.Number;
import com.wsti.calculator.model.token.function.*;

import java.util.Objects;

public final class Functions {

    private Functions() {}

    public static boolean isFunction(String token) {
        try {
            Objects.requireNonNull(Functions.of(token));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static Function of(String token) {
        Number argument = getFunctionArgument(token);

        switch (getFunctionName(token)) {
            case "sqrt":
                return new SqrtFunction(argument);
            case "fact":
                return new FactFunction(argument);
            case "sqr":
                return new SqrFunction(argument);
            case "log":
                return new LogFunction(argument);
            case "ln":
                return new LnFunction(argument);
            case "abs":
                return new AbsFunction(argument);
            case "sin":
                return new SinFunction(argument);
            case "cos":
                return new CosFunction(argument);
            case "tg":
                return new TgFunction(argument);
            case "ctg":
                return new CtgFunction(argument);
            default:
                throw new IllegalArgumentException("Token: " + token + " is not a function");
        }
    }

    private static String getFunctionName(String token) {
        return token.substring(0, token.indexOf('('));
    }

    private static Number getFunctionArgument(String token) {
        String value = token.substring(token.indexOf('(') + 1, token.indexOf(')'));
        if (Numbers.isNumber(value)) {
            return new Number(value);
        }
        throw new IllegalArgumentException("Missing function argument in token: " + token);
    }

    public static String toToken(String functionName, String functionArgument) {
        return String.format("%s(%s)", functionName, functionArgument);
    }
}
