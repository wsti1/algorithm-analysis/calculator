package com.wsti.calculator.model.token.util;

import com.wsti.calculator.model.token.Token;

import java.util.Collection;
import java.util.stream.Collectors;

public final class Strings {

    private Strings() {}

    public static String join(String delimiter, Collection<? extends Token> tokens) {
        return tokens.stream()
                .map(Token::toString)
                .collect(Collectors.joining(delimiter));
    }
}
