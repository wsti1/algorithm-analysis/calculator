package com.wsti.calculator.model.token.function;

import com.wsti.calculator.model.token.Function;
import com.wsti.calculator.model.token.Number;

public class LogFunction extends Function {

    public LogFunction(Number argument) {
        super("log", argument);
    }

    @Override
    public Number get() {
        return new Number(Math.log10(argument.getValue().doubleValue()));
    }
}