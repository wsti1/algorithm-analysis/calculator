package com.wsti.calculator.model.token;

import java.util.function.Supplier;

public abstract class Function extends Token implements Supplier<Number> {

    protected Number argument;

    public Function(String identifier, Number argument) {
        super(identifier);
        this.argument = argument;
    }

    public void setArgument(Number argument) {
        this.argument = argument;
    }

    public Number getArgument() {
        return argument;
    }

    @Override
    public String toString() {
        return String.format("%s(%s)", identifier, argument);
    }
}
