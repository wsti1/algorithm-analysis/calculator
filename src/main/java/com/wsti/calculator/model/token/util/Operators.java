package com.wsti.calculator.model.token.util;

import com.wsti.calculator.model.token.Operator;
import com.wsti.calculator.model.token.operator.*;

import java.util.ArrayList;
import java.util.List;

public final class Operators {

    private static final List<Operator> operatorList = new ArrayList<>();

    static {
        operatorList.add(new AddOperator());
        operatorList.add(new SubtractOperator());
        operatorList.add(new MultiplyOperator());
        operatorList.add(new DivideOperator());
        operatorList.add(new PowerOperator());
        operatorList.add(new PercentageOperator());
    }

    private Operators() {}

    public static Operator of(String token) {
        return operatorList.stream()
                .filter(operator -> operator.getIdentifier().equals(token))
                .findAny()
                .orElseThrow(()-> new IllegalArgumentException("Token: " + token + " is not a operator"));
    }

    public static boolean isOperator(String token) {
        return operatorList.stream()
                .anyMatch(operator -> operator.getIdentifier().equals(token));
    }

    public static boolean isNotParenthesis(Operator target) {
        return operatorList.stream()
                .anyMatch(operator -> operator.equals(target) && !"()".contains(operator.getIdentifier()));
    }
}
