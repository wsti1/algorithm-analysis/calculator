package com.wsti.calculator.model.token.function;

import com.wsti.calculator.model.token.Function;
import com.wsti.calculator.model.token.Number;

public class LnFunction extends Function {

    public LnFunction(Number argument) {
        super("ln", argument);
    }

    @Override
    public Number get() {
        return new Number(Math.log(argument.getValue().doubleValue()));
    }
}