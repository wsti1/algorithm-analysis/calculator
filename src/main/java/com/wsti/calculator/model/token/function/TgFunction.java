package com.wsti.calculator.model.token.function;

import com.wsti.calculator.model.token.Function;
import com.wsti.calculator.model.token.Number;

import java.math.BigDecimal;

public class TgFunction extends Function {

    public TgFunction(Number argument) {
        super("tg", argument);
    }

    @Override
    public Number get() {
        BigDecimal degrees = argument.getValue();
        double radians = Math.toRadians(degrees.doubleValue());
        return new Number(Math.tan(radians));
    }
}