package com.wsti.calculator.model.token.function;

import com.wsti.calculator.model.token.Function;
import com.wsti.calculator.model.token.Number;

import java.math.MathContext;

public class SqrtFunction extends Function {

    public SqrtFunction(Number argument) {
        super("sqrt", argument);
    }

    @Override
    public Number get() {
        return new Number(argument.getValue().sqrt(MathContext.DECIMAL32));
    }
}