package com.wsti.calculator.model.token.parenthesis;

import com.wsti.calculator.model.token.Parenthesis;

public class OpenParenthesis extends Parenthesis {

    public OpenParenthesis() {
        super("(");
    }
}
