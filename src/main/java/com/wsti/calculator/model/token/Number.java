package com.wsti.calculator.model.token;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

public class Number extends Token {

    private BigDecimal value;

    public Number(String character) {
        super(character);
        this.value = new BigDecimal(character);
    }

    public Number(Double value) {
        super(value.toString());
        this.value = new BigDecimal(value, MathContext.DECIMAL32);
    }

    public Number(BigDecimal value) {
        super(value.toString());
        this.value = value;
    }

    public BigDecimal getValue() {
        return value;
    }

    public BigInteger getBigIntValue() {
        return value.toBigInteger();
    }
}
