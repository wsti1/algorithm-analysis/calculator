package com.wsti.calculator.model.token;

public class Parenthesis extends Token {

    public Parenthesis(String identifier) {
        super(identifier);
    }
}
