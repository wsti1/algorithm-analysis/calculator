package com.wsti.calculator.model.token.operator;

import com.wsti.calculator.model.token.Number;
import com.wsti.calculator.model.token.Operator;

import java.math.BigDecimal;
import java.math.MathContext;

public class PercentageOperator extends Operator {

    public PercentageOperator() {
        super("%", 2);
    }

    @Override
    public Number apply(Number firstNumber, Number secondNumber) {
        BigDecimal firstValue = firstNumber.getValue();
        BigDecimal secondValue = secondNumber.getValue();

        return new Number(firstValue.divide(secondValue, MathContext.DECIMAL32).multiply(BigDecimal.valueOf(100L)));
    }
}
