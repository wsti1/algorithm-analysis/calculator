package com.wsti.calculator.model.token;

import java.util.function.BinaryOperator;

public abstract class Operator extends Token implements BinaryOperator<Number> {

    private Integer priority;

    public Operator(String identifier, Integer priority) {
        super(identifier);
        this.priority = priority;
    }

    public Integer getPriority() {
        return priority;
    }
}
