package com.wsti.calculator.model.token.operator;

import com.wsti.calculator.model.token.Number;
import com.wsti.calculator.model.token.Operator;

import java.math.BigDecimal;

public class SubtractOperator extends Operator {

    public SubtractOperator() {
        super("-", 1);
    }

    @Override
    public Number apply(Number firstNumber, Number secondNumber) {
        BigDecimal firstValue = firstNumber.getValue();
        BigDecimal secondValue = secondNumber.getValue();

        return new Number(firstValue.subtract(secondValue));
    }
}