package com.wsti.calculator.model.exception;

public class ParserException extends Exception {

    public ParserException(String message) {
        super(message);
    }
}
