package com.wsti.calculator.model.exception;

public class InterpreterException extends Exception {

    public InterpreterException(String message) {
        super(message);
    }
}
