package com.wsti.calculator.model.exception;

public class LexerException extends Exception {

    public LexerException(String message) {
        super(message);
    }
}
